package com.gitlab.arnekeller.slippy

data class App(val pkg: String, val name: String, var launches: Int, var hidden: Boolean)