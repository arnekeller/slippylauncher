/*
 * Slippy Launcher
 * Copyright (C) 2019 Arne Keller
 * Partially inspired by HenriDellal's emerald launcher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.gitlab.arnekeller.slippy

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

object DatabaseHandler {
	private const val DB_NAME = "AppsDB5"
	private const val TABLE_NAME = "apps"
	private const val ID = "id"
	private const val PACKAGE = "pkg"
	private const val NAME = "name"
	private const val LAUNCHES = "launches"
	private const val HIDDEN = "hidden"
	private var db: SQLiteDatabase? = null
	private var dbLock: Lock = ReentrantLock()
	var dirty: Boolean = false

	fun init(ctx: Context) {
		dbLock.lock()
		if (db == null) {
			val filePath = ctx.getDatabasePath(DB_NAME)
			db = SQLiteDatabase.openOrCreateDatabase(filePath, null)
			db!!.execSQL("CREATE TABLE IF NOT EXISTS $TABLE_NAME ($ID Integer PRIMARY KEY UNIQUE, $PACKAGE TEXT UNIQUE NOT NULL, $NAME TEXT NOT NULL, $LAUNCHES INT NOT NULL)")
			maybeMigrate()
		}
		dbLock.unlock()
	}

	private fun maybeMigrate() {
		val cursor = db!!.query(TABLE_NAME, null, null, null, null, null, null)
		val columns = cursor.columnNames
		cursor.close()
		if (!columns.contains(HIDDEN)) {
			db!!.execSQL("ALTER TABLE $TABLE_NAME ADD COLUMN $HIDDEN INTEGER NOT NULL DEFAULT 0")
		}
	}

	fun insertApp(pkg: String, name: String, launches: Int) : Boolean {
		dbLock.lock()
		val values = ContentValues()
		values.put(PACKAGE, pkg)
		values.put(NAME, name)
		values.put(LAUNCHES, launches)
		values.put(HIDDEN, 0)
		val success = db!!.insert(TABLE_NAME, null, values)
		dbLock.unlock()
		return success != -1L
	}

	fun addLaunch(app: App) {
		dbLock.lock()
		val values = ContentValues()
		values.put(LAUNCHES, app.launches+1)
		db!!.update(TABLE_NAME, values, "$PACKAGE=?", arrayOf(app.pkg))
		dirty = true
		dbLock.unlock()
	}

	fun hide(app: App) {
		dbLock.lock()
		val values = ContentValues()
		values.put(HIDDEN, 1)
		db!!.update(TABLE_NAME, values, "$PACKAGE=?", arrayOf(app.pkg))
		dbLock.unlock()
	}

	fun removeApp(pkg: String) {
		db!!.delete(TABLE_NAME, "$PACKAGE=?", arrayOf(pkg))
	}

	fun hasApp(pkg: String) : Boolean {
		val sql = "SELECT $ID FROM $TABLE_NAME WHERE $PACKAGE = \"$pkg\" LIMIT 1"
		val cursor = db!!.rawQuery(sql, null)
		val result = cursor?.moveToFirst() ?: false
		cursor?.close()
		return result
	}

	fun countVisibleApps() : Int {
		val cursor = db!!.rawQuery("SELECT * FROM $TABLE_NAME WHERE $HIDDEN = 0", null)
		val amount = cursor.count // TODO: use SELECT COUNT instead
		cursor.close()
		return amount
	}

	fun allApps() : MutableList<App> {
		val all: MutableList<App> = ArrayList()
		val selectALLQuery = "SELECT * FROM $TABLE_NAME"
		val cursor = db!!.rawQuery(selectALLQuery, null)
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					//val id = cursor.getString(cursor.getColumnIndex(ID))
					val pkg = cursor.getString(cursor.getColumnIndex(PACKAGE))
					val name = cursor.getString(cursor.getColumnIndex(NAME))
					val launches = cursor.getInt(cursor.getColumnIndex(LAUNCHES))
					val hidden = cursor.getInt(cursor.getColumnIndex(HIDDEN)) != 0
					all.add(App(pkg, name, launches, hidden))
				} while (cursor.moveToNext())
			}
		}
		cursor.close()
		return all
	}

	fun deleteAll() {
		db!!.delete(TABLE_NAME, null, null)
	}
}