/*
 * Slippy Launcher
 * Copyright (C) 2019 Arne Keller
 * Partially inspired by HenriDellal's emerald launcher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.gitlab.arnekeller.slippy

import androidx.appcompat.app.AppCompatActivity
import android.widget.RelativeLayout
import android.content.Context
import android.view.*
import android.view.MotionEvent.ACTION_DOWN
import android.view.MotionEvent.ACTION_UP
import android.app.ProgressDialog
import android.widget.ImageView
import kotlin.math.absoluteValue
import android.graphics.drawable.Drawable
import java.io.FileInputStream
import android.content.Intent
import android.graphics.Point
import android.os.*
import android.util.DisplayMetrics
import android.util.TypedValue
import android.widget.Toast
import android.content.ActivityNotFoundException
import android.content.ComponentName
import android.content.pm.LauncherActivityInfo
import android.content.pm.LauncherApps
import android.content.DialogInterface
import android.net.Uri
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity(), View.OnTouchListener, GestureDetector.OnGestureListener, Choreographer.FrameCallback, ViewTreeObserver.OnGlobalLayoutListener {
	private var detector: GestureDetector? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		val v = findViewById<RelativeLayout>(R.id.map)
		v.rootView.setOnTouchListener(this)
		v.rootView.viewTreeObserver.addOnGlobalLayoutListener(this)
		detector = GestureDetector(this, this)
		DatabaseHandler.init(this)

		resetOffsets()
		if (DatabaseHandler.allApps().isEmpty()) {
			rescanApps(this)
		} else {
			initLayout()
		}
	}

	companion object {
		private var appThread: GetAppsThread? = null
		var rescan: Boolean = false
		private var drawables: MutableMap<String, Drawable> = HashMap()

		fun rescanApps(ctx: MainActivity) {
			if (appThread?.isAlive ?: false) {
				return
			}
			val progress = ProgressDialog(ctx)
			progress.setCancelable(false)
			progress.setMessage(ctx.resources.getString(R.string.loading_apps))
			progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
			progress.isIndeterminate = true
			progress.show()
			val handler = object : Handler() {
				override fun handleMessage(msg: Message) {
					try {
						progress.dismiss()
					} catch (e: Exception) {
					}
					ctx.initLayout()
					appThread?.quit()
				}
			}
			val appsTask: Runnable
			if (Build.VERSION.SDK_INT >= 21) {
				appsTask = object : Runnable {
					override fun run() {
						val main = Intent(Intent.ACTION_MAIN, null)
						main.addCategory(Intent.CATEGORY_LAUNCHER)
						//val list = ctx.packageManager.queryIntentActivities(main, 0)
						val service = ctx.getSystemService(Context.LAUNCHER_APPS_SERVICE)
						val list: List<LauncherActivityInfo>
						if (service is LauncherApps) {
							list = service.getActivityList(null, Process.myUserHandle())
						} else {
							handler.sendEmptyMessage(0)
							return
						}
						progress.isIndeterminate = false
						progress.max = list.size

						var i = 0
						val dm = DisplayMetrics()
						ctx.windowManager.defaultDisplay.getMetrics(dm)
						while (i < list.size) {
							progress.progress = i

							val info = list[i]
							val pkg = info.componentName.flattenToString()
							if (DatabaseHandler.hasApp(pkg)) {
								i++
								continue
							}
							val iconFile = IconCache.getIconFile(ctx, pkg)
							if (!iconFile.exists()) {
								IconCache.writeIconToFile(iconFile, info.getIcon(dm.densityDpi))
							}
							val name = info.label.toString()
							DatabaseHandler.insertApp(pkg, name, 0)
							i++
						}
						val currentApps = DatabaseHandler.allApps()
						for (a in currentApps) {
							var found = false
							for (info in list) {
								if (info.componentName.flattenToString() == a.pkg) {
									found = true
									break
								}
							}
							if (!found) {
								DatabaseHandler.removeApp(a.pkg)
							}
						}
						handler.sendEmptyMessage(0)
					}
				}
				appThread = GetAppsThread("GetAppsThread")
				appThread!!.start()
				appThread!!.prepareHandler()
				appThread!!.postTask(appsTask)
			} else {
				// TODO: figure out how Emerald Launcher does it
			}
		}

		fun calcPosition(c: Int) : Point {
			if (c == 0) {
				return Point(0, 0)
			}
			var ring = 1
			var end = Point(-1, ring)
			var pos = Point(0, ring)
			for (i in 2..c) {
				if (pos == end) {
					ring++
					end = Point(-1, ring)
					pos = Point(0, ring)
				} else {
					if (pos.x == -ring && pos.y < ring) {
						pos.y++
					} else if (pos.x == -ring && pos.y == ring) {
						pos.x++
					} else if (pos.y == ring && pos.x < ring) {
						pos.x++
					} else if (pos.y == ring && pos.x == ring) {
						pos.y--
					} else if (pos.y > -ring && pos.x == ring) {
						pos.y--
					} else if (pos.y == -ring && pos.x == ring) {
						pos.x--
					} else if (pos.y == -ring && pos.x > -ring) {
						pos.x--
					} else if (pos.y == -ring && pos.x == ring) {
						pos.y++
					}
				}
			}
			return pos
		}
	}

	override fun onResume() {
		super.onResume()
		resetOffsets()
		setOffsets()
		maybeRescan()
	}

	private fun maybeRescan() {
		if (rescan) {
			rescan = false
			rescanApps(this)
			DatabaseHandler.dirty = false
			initLayout()
		} else if (DatabaseHandler.dirty) {
			DatabaseHandler.dirty = false
			initLayout()
		}
	}

	private fun initLayout() {
		val parent = findViewById<RelativeLayout>(R.id.map)
		parent.removeAllViews()

		doAsync {
			val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
			val items: MutableList<RelativeLayout> = ArrayList()
			val dm = DisplayMetrics()
			windowManager.defaultDisplay.getMetrics(dm)
			val size = TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP,
				48.0f,
				dm
			).toDouble()
			val apps = DatabaseHandler.allApps()
			apps.sortByDescending { it.launches }
			var c = 0
			for (a in apps) {
				if (a.hidden) {
					continue
				}
				val custom = inflater.inflate(R.layout.item_app, parent, false) as RelativeLayout
				val img = custom.findViewById<ImageView>(R.id.icon)
				var iconFile = IconCache.getCustomIconFile(this, a)
				if (!iconFile.exists()) {
					iconFile = IconCache.getIconFile(this, a)
				}
				if (iconFile.exists()) {
					try {
						var drawable = drawables[a.pkg]
						if (drawable == null) {
							drawable = Drawable.createFromStream(FileInputStream(iconFile), null)
							drawables[a.pkg] = drawable
						}
						img.setImageDrawable(drawable)
					} catch (e: Exception) {
						img.setImageResource(android.R.drawable.sym_def_app_icon)
					}
				} else {
					img.setImageResource(android.R.drawable.sym_def_app_icon)
				}
				custom.measure(0, 0)
				val pos = calcPosition(c)
				c++
				custom.top = OFFSET_Y + (pos.y * size).toInt()
				custom.left = OFFSET_X + (pos.x * size).toInt()
				items.add(custom)
				if (items.size > 10) {
					runOnUiThread {
						for (x in items) {
							val params = RelativeLayout.LayoutParams(x.measuredWidth, x.measuredHeight)
							params.leftMargin = x.left
							params.topMargin = x.top
							x.left = 0
							x.top = 0
							parent.addView(x, params)
						}
						items.clear()
					}
					while (items.size > 0) {
						// wait
					}
				}
			}
			runOnUiThread {
				for (x in items) {
					val params = RelativeLayout.LayoutParams(x.measuredWidth, x.measuredHeight)
					params.leftMargin = x.left
					params.topMargin = x.top
					x.left = 0
					x.top = 0
					parent.addView(x, params)
				}
				items.clear()
			}
		}.execute()
	}

	private var lastX: Float? = null
	private var lastY: Float? = null
	private val SPEED_FACTOR = 2.3f
	private val OFFSET_X = 2000
	private val OFFSET_Y = 2000
	private var offsetX = 0.0f
	private var offsetY = 0.0f

	private fun resetOffsets() {
		val dm = DisplayMetrics()
		windowManager.defaultDisplay.getMetrics(dm)
		offsetX = dm.widthPixels.toFloat() / 2.0f
		offsetY = dm.heightPixels.toFloat() / 2.0f
		val size = TypedValue.applyDimension(
			TypedValue.COMPLEX_UNIT_DIP,
			24.0f,
			dm
		)
		offsetX -= size
		offsetY -= size
		offsetY -= size * 1.3f
	}

	override fun onGlobalLayout() {
		setOffsets()
	}

	private fun setOffsets() {
		val view = findViewById<RelativeLayout>(R.id.map)
		view.top = -OFFSET_Y + offsetY.toInt()
		view.left = -OFFSET_X + offsetX.toInt()
	}

	override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
		returning = false
		returnX = null
		returnY = null
		slinging = false
		val lastX = lastX
		val lastY = lastY
		if (lastX != null && lastY != null && e2 != null) {
			offsetX += (e2.x - lastX) * SPEED_FACTOR
			offsetY += (e2.y - lastY) * SPEED_FACTOR
			setOffsets()
			this.lastX = e2.x
			this.lastY = e2.y
		}
		return true
	}

	private var slingX = 0.0f
	private var slingY = 0.0f
	private val SLING_SPEED_FACTOR = 1.0f
	private val SLING_FRICTON = 0.93f

	override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
		returning = false
		slinging = true
		slingX = e2.x - e1.x
		slingY = e2.y - e1.y
		Choreographer.getInstance().postFrameCallback(this)

		return true
	}

	private var slinging = false
	private var returning = false
	private var returnX: Float? = null
	private var returnY: Float? = null

	override fun doFrame(frameTimeNanos: Long) {
		if (slinging) {
			offsetX += (slingX * SLING_SPEED_FACTOR) / 5.0f
			offsetY += (slingY * SLING_SPEED_FACTOR) / 5.0f
			setOffsets()

			slingX *= SLING_FRICTON
			slingY *= SLING_FRICTON

			if (slingX.absoluteValue > 2.0 || slingY.absoluteValue > 2.0) {
				Choreographer.getInstance().postFrameCallback(this)
			} else {
				slinging = false
				returning = true
				Choreographer.getInstance().postFrameCallback(this)
			}
		} else if (returning) {
			val view = findViewById<RelativeLayout>(R.id.map)
			val dm = DisplayMetrics()
			windowManager.defaultDisplay.getMetrics(dm)
			val size = TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP,
				24.0f,
				dm
			)
			if (returnX == null || returnY == null) {
				val currentPoint = Point(offsetX.toInt(), offsetY.toInt())
				currentPoint.x -= dm.widthPixels / 2
				currentPoint.y -= dm.heightPixels / 2
				currentPoint.x += size.toInt()
				currentPoint.y += (size * 2.3f).toInt()
				var lowestDistance = Int.MAX_VALUE
				var bestPoint = Point(0, 0)
				for (i in 0 until DatabaseHandler.countVisibleApps()) {
					val pos = calcPosition(i)
					pos.x *= -size.toInt() * 2
					pos.y *= -size.toInt() * 2
					val dist = (pos.x-currentPoint.x)*(pos.x-currentPoint.x) + (pos.y-currentPoint.y)*(pos.y-currentPoint.y)
					if (dist < lowestDistance) {
						lowestDistance = dist
						bestPoint = pos
					}
				}
				returnX = bestPoint.x.toFloat() + dm.widthPixels / 2 - size
				returnY = bestPoint.y.toFloat() + dm.heightPixels / 2 - 2.3f * size
				Choreographer.getInstance().postFrameCallback(this)
			} else {
				offsetX += (returnX!! - offsetX) * 0.5f
				offsetY += (returnY!! - offsetY) * 0.5f
				setOffsets()
				if ((returnY!! - offsetY).absoluteValue > 0.5f || (returnX!! - offsetX).absoluteValue > 0.5f) {
					Choreographer.getInstance().postFrameCallback(this)
				} else {
					// done!
					returning = false
					returnX = null
					returnY = null
				}
			}
		}
	}

	override fun onDown(e: MotionEvent?): Boolean {
		return true
	}

	override fun onSingleTapUp(e: MotionEvent?): Boolean {
		if (e == null) {
			return true
		}
		val a = findApp(e) ?: return true
		DatabaseHandler.addLaunch(a)
		val i = Intent(Intent.ACTION_MAIN)
		i.addCategory(Intent.CATEGORY_LAUNCHER)
		i.component = ComponentName.unflattenFromString(a.pkg)
		i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
		try {
			startActivity(i)
		} catch (e: ActivityNotFoundException) {
			Toast.makeText(this, "Activity not found", Toast.LENGTH_LONG).show()
			DatabaseHandler.removeApp(a.pkg)
			initLayout()
		}
		return true
	}

	override fun onShowPress(e: MotionEvent?) {
	}

	override fun onLongPress(e: MotionEvent?) {
		if (e == null) {
			return
		}
		val a = findApp(e) ?: return
		val builder = AlertDialog.Builder(this)

		builder.setTitle(a.name)
		builder.setCancelable(true)

		val commands = arrayOf(resources.getString(R.string.about_title), resources.getString(R.string.find_in_market), resources.getString(R.string.uninstall), resources.getString(R.string.remove_from_launcher))
		builder.setAdapter(ArrayAdapter(this, android.R.layout.simple_list_item_1, commands),
			DialogInterface.OnClickListener { _, which ->
				val uri: Uri
				when (which) {
					0 -> {
						uri = Uri.parse(
							"package:" + ComponentName.unflattenFromString(a.pkg)!!.packageName
						)
						startActivity(Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, uri))
					}
					1 -> {
						uri = Uri.parse(("market://details?id=" + ComponentName.unflattenFromString(a.pkg)!!.packageName))
						startActivity(Intent(Intent.ACTION_VIEW, uri))
					}
					2 -> {
						uri = Uri.parse(("package:" + ComponentName.unflattenFromString(a.pkg)!!.packageName))
						startActivityForResult(Intent(Intent.ACTION_DELETE, uri), 0)
					}
					3 -> {
						DatabaseHandler.hide(a)
						initLayout()
					}
				}
			})
		if (!isFinishing) {
			builder.create().show()
		}
	}

	private fun findApp(e: MotionEvent): App? {
		val dm = DisplayMetrics()
		windowManager.defaultDisplay.getMetrics(dm)
		val size = TypedValue.applyDimension(
			TypedValue.COMPLEX_UNIT_DIP,
			48.0f,
			dm
		).toDouble()
		val x = e.x - offsetX
		val y = e.y - offsetY
		val apps = DatabaseHandler.allApps()
		apps.sortByDescending { it.launches }
		var c = 0
		for (a in apps) {
			if (a.hidden) {
				continue
			}
			val pos = calcPosition(c)
			c++
			if (pos.x * size < x && pos.x * size + size > x && pos.y * size < y && pos.y * size + size > y) {
				return a
			}
		}
		return null
	}

	override fun onTouch(v: View?, event: MotionEvent?): Boolean {
		if (event?.action == ACTION_UP) {
			lastX = null
			lastY = null
			returning = true
			Choreographer.getInstance().postFrameCallback(this)
		} else if (event?.action == ACTION_DOWN) {
			slingX = 0.0f
			slingY = 0.0f
			lastX = event.x
			lastY = event.y
		}
		detector?.onTouchEvent(event)
		return true
	}
}

class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
	override fun doInBackground(vararg params: Void?): Void? {
		handler()
		return null
	}
}
