/*
 * Slippy Launcher
 * Copyright (C) 2019 Arne Keller
 * Partially inspired by HenriDellal's emerald launcher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.gitlab.arnekeller.slippy

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class PackageStateChangedReceiver : BroadcastReceiver() {
	override fun onReceive(context: Context?, intent: Intent) {
		if (context == null)
			return
		val action = intent.action
		DatabaseHandler.dirty = true
		try {
			if (action == Intent.ACTION_PACKAGE_REMOVED) {
				onPackageRemove(intent)
			} else if (action == Intent.ACTION_PACKAGE_REPLACED) {
				onPackageReplace(intent)
			} else {
				onPackageAdd()
			}
		} catch (t: Throwable) {
			t.printStackTrace()
		}
	}

	private fun onPackageReplace(intent: Intent) {
		// TODO: maybe use intent.getStringArrayExtra(Intent.EXTRA_CHANGED_COMPONENT_NAME_LIST
		// ^ seems to be null sometimes?
		MainActivity.rescan = true
	}

	private fun onPackageRemove(intent: Intent) {
		if (intent.getBooleanExtra(Intent.EXTRA_REPLACING, false)) {
			return
		}
		val packageName = intent.data!!.schemeSpecificPart
		DatabaseHandler.removeApp(packageName)
	}

	private fun onPackageAdd() {
		MainActivity.rescan = true
	}
}