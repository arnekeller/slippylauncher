package com.gitlab.arnekeller.slippy

import android.os.Handler
import android.os.HandlerThread

class GetAppsThread(name: String) : HandlerThread(name) {
	private var mHandler: Handler? = null

	fun postTask(task: Runnable) {
		mHandler!!.post(task)
	}

	fun prepareHandler() {
		mHandler = Handler(looper)
	}
}