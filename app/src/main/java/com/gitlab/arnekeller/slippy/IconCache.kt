/*
 * Slippy Launcher
 * Copyright (C) 2019 Arne Keller
 * Partially inspired by HenriDellal's emerald launcher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.gitlab.arnekeller.slippy

import android.content.Context
import android.net.Uri
import java.io.*
import android.graphics.Bitmap.CompressFormat
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.toBitmap

object IconCache {
	fun writeIconToFile(iconFile: File, d: Drawable) {
		try {
			val bmp = d.toBitmap()
			//bmp = Bitmap.createScaledBitmap(bmp, 64, 64, false)
			// save icon in cache
			val out = FileOutputStream(iconFile)
			bmp.compress(CompressFormat.PNG, 100, out)
			out.close()
		} catch (e: Exception) {
			iconFile.delete()
		}
	}

	fun genFilename(c: Context, name: String): String {
		val dir = c.cacheDir
		return dir.path + "/" + name + ".MyCache"
	}

	fun getCustomIconFile(c: Context, component: String): File {
		return File(
			c.filesDir,
			Uri.encode(component) + ".png"
		)
	}

	fun getCustomIconFile(c: Context, data: App): File {
		return File(
			c.filesDir,
			getIconFileName(data, ".png")
		)
	}

	fun getShortcutIconFileName(uri: String): String {
		return uri.hashCode().toString() + ".png"
	}

	fun getIconFileName(id: String): String {
		return Uri.encode(id) + ".icon.png"
	}

	fun getIconFileName(data: App): String {
		return getIconFileName(data, ".icon.png")
	}

	fun getIconFileName(data: App, postfix: String): String {
		val component = data.pkg
		return Uri.encode(component) + postfix
	}

	fun getShortcutIconFile(c: Context, uri: String): File {
		return File(c.filesDir, getShortcutIconFileName(uri))
	}

	fun getIconFile(c: Context, component: String): File {
		return File(c.filesDir, getIconFileName(component))
	}

	fun getIconFile(c: Context, data: App): File {
		return File(c.filesDir, getIconFileName(data))
	}

	fun deleteIcon(c: Context, app: App) {
		getIconFile(c, app).delete()
	}

	/* removes icons of deleted apps */
	fun cleanIcons(c: Context, data: ArrayList<App>) {
		val dirs = c.filesDir.listFiles()
		for (f in dirs) {
			var deleteFile = true
			for (a in data) {
				if (getIconFileName(a) == f.name) {
					deleteFile = false
					break
				}
			}
			if (deleteFile && f.name.contains(".icon.png")) {
				f.delete()
			}
		}
	}

	fun deleteIcons(c: Context) {
		val dirs = c.filesDir.listFiles()

		for (f in dirs) {
			if (f.name.endsWith(".icon.png")) {
				f.delete()
			}
		}

	}
}